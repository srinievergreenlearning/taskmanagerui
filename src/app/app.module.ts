import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms'
import { AppRoutingModule,routerComponents } from './app-routing.module';
import { AppComponent } from './app.component';

import { TaskPipe } from './filter/task-pipe';
import { SharedService } from './service/shared.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
   routerComponents,
   TaskPipe
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule 
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
