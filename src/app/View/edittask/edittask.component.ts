import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/service/shared.service';
import { ActivatedRoute } from '@angular/router';
import { Task } from 'src/app/Model/task-model';
import { DatePipe } from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edittask/:id',
  templateUrl: './edittask.component.html',
  //styleUrls: ['./edittask.component.css']
  styles: [`input.ng-invalid{border-left:5px solid red;}
            input.ng-valid{border-left: 5px solid green;}`]
})
export class EdittaskComponent implements OnInit {

  constructor(private _taskService:SharedService, private _route:ActivatedRoute, private _nav: Router) { }
  public id: any;
  public taskIds=[];
  public task : Task={
    TaskId:null,
    ParentId:null,
    Task_Name:null,
    StartDate:null ,
    EndDate:null,
    Priority:null
  }

  pipe = new DatePipe('en-US');

  ngOnInit() {
    this._taskService.getTasks()
      .subscribe(data => this.taskIds = data);
        console.log(this.taskIds);

     
    this.id=this._route.snapshot.paramMap.get("id");
    this._taskService.getTaskById(this.id)
      .subscribe(data => {
        this.task = data; 
        this.task.StartDate = this.pipe.transform(data.StartDate,'yyyy-MM-dd'); 
        this.task.EndDate = this.pipe.transform(data.EndDate,'yyyy-MM-dd'); 
        console.log(data); 
        console.log(this.pipe.transform(data.StartDate,'yyyy-MM-dd'));     
      });
  }

  onSubmit(taskform : Task){
    console.log(taskform); 
        this._taskService.updateTask(taskform)
      .subscribe(data => console.log(data),
      error => console.log(error),
      ()=> this._nav.navigate(['/app-listtask'])
    );
  }
}

