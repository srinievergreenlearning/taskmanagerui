import { Component, OnInit } from '@angular/core';
import { TaskPipe} from 'src/app/filter/task-pipe';
import { Router} from '@angular/router';
import { SharedService } from 'src/app/service/shared.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-listtask',
  templateUrl: './listtask.component.html',
  styleUrls: ['./listtask.component.css']
})
export class ListtaskComponent implements OnInit {
 constructor(private _route:Router, private _taskService : SharedService) { }
 public taskLists =[];
 public taskIDSearch: string;
 public taskSearch: string;
 public parentSearch: string;
 public prioritySearch: string;
 public startDateSearch: string;
 public endDateSearch: string;

  
  pipe = new DatePipe('en-US');
  public currentDate = this.pipe.transform(new Date(), 'yyyy-MM-dd');
  public endDate;

  ngOnInit() {
    this._taskService.getTasks()
                    .subscribe(data => this.taskLists=data);
                    console.log(this.taskLists);
                    console.log(this.currentDate);
  }

  dateValidate(enddt){
    //this.endDate =  this.pipe.transform(enddt, 'yyyy-MM-dd');
    this.endDate =enddt;
    console.log(this.endDate <= this.currentDate);
    if (this.endDate <= this.currentDate)
      return true;
    else
      return false;
  }

  onSelect(Task){
    this._route.navigate(['/app-edittask',Task.TaskId]);
  }

  onEndTask(task){
    this._taskService.endTask(task).subscribe(data=>console.log(data));
  }

  onDelete(task){
    this._taskService.deleteTask(task).
        subscribe(data=> console.log(data),
        error => console.log(error),
        ()=> this._taskService.getTasks()
          .subscribe(data=> data => this.taskLists=data))

          this._taskService.getTasks()
                    .subscribe(data => this.taskLists=data);
                    console.log(this.taskLists);
                    console.log(this.currentDate);
  }
  
}
//constructor() { }

  // ngOnInit() {
  // }

//}
