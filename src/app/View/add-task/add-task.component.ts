import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/service/shared.service';
import { ActivatedRoute } from '@angular/router';
import { Task } from 'src/app/Model/task-model';
import { DatePipe } from '@angular/common';
import { Router} from '@angular/router';


@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  //styleUrls: ['./add-task.component.css']
  styles: [`input.ng-untouched{border-left:5px solid red;}
            input.ng-pristine{border-left:5px solid red;}
            input.ng-touched{border-left: 5px solid green;}
            input.ng-dirty{border-left:5px solid green;}`]
})
export class AddTaskComponent implements OnInit {

  constructor(private _taskService:SharedService, private _route:ActivatedRoute, private _nav: Router) { }
  public id: any;
  public taskIds=[];
  public task : Task={
    TaskId:null,
    ParentId:null,
    Task_Name:null,
    StartDate:null ,
    EndDate:null,
    Priority:null
  }
  
  pipe = new DatePipe('en-US');
  
  ngOnInit() {
    this._taskService.getTasks()
      .subscribe(data => this.taskIds = data);
        console.log(this.taskIds);
  }
  onSubmit(taskform : Task){
    console.log(taskform); 
    this._taskService.addTask(taskform)
      .subscribe(data => console.log(data),
      error => console.log(error),
      ()=> this._nav.navigate(['/app-listtask'])
    );
  }
}
