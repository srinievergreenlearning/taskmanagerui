import { Injectable } from '@angular/core';
import {  HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../Model/task-model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private _http : HttpClient) { }

getTasks() : Observable<Task[]>{
  return this._http.get<Task[]>('http://localhost:35902/api/task/GetTasks');
}

getTaskIds() : Observable<Task[]>{
  return this._http.get<Task[]>('http://localhost:35902/api/task/GetTaskIds');
}

getTaskById(id : number) : Observable<Task>{
  return this._http.get<Task>('http://localhost:35902/api/task/GetTaskById/'+id);
}

addTask(task : Task) : Observable<Task>{
  return this._http.post<Task>('http://localhost:35902/api/task/AddTask',task);
}

updateTask(task : Task) : Observable<Task>{
  return this._http.post<Task>('http://localhost:35902/api/task/UpdateTask/',task);
}

deleteTask(task:Task):Observable<any>{
  return this._http.post('http://localhost:35902/api/task/DeleteTask', task);
}

endTask(task : Task):Observable<any>{
  return this._http.post('http://localhost:35902/api/task/EndTask',task);
}

}
