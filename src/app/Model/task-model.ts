export class Task {
    public TaskId : number;
    public Task_Name : string;
    public ParentId : number;
    // public ParentTaskDescription : string;
    public Priority : number;
    public StartDate : string;
    public EndDate : string;
}
