import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class TaskPipe implements PipeTransform {

  transform(tasks: any, taskIDSearch : number, taskSearch : string ,parentSearch : number ,prioritySearch : number,startDateSearch : string,endDateSearch : string ): any {

    if (taskIDSearch || taskSearch || parentSearch || prioritySearch || startDateSearch || endDateSearch){
       
      return tasks.filter(item =>{

        if (taskIDSearch && item.TaskId != taskIDSearch){
          return false;
      }
         
          if (taskSearch && item.Task_Name.indexOf(taskSearch)== -1){
            return false;
        }
        
          if (parentSearch && item.ParentId != parentSearch){
              return false;
          }
          if (prioritySearch && item.Priority != prioritySearch){
              return false;
          }
          if (startDateSearch && item.StartDate.indexOf(startDateSearch) == -1){
            return false;
          }
          if (endDateSearch && item.EndDate.indexOf(endDateSearch)== -1){
            return false;
          }
    return true;
     })
    }
    else{
      return tasks;
    }
  }

}
