import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTaskComponent} from './View/add-task/add-task.component';
import {EdittaskComponent} from './View/edittask/edittask.component';
import { ListtaskComponent } from './View/listtask/listtask.component';
import {ViewtaskComponent} from './View/viewtask/viewtask.component';

const routes: Routes = [
  {path:"",redirectTo:"/app-listtask",pathMatch:"full"},
  {path:"app-add-task",component:AddTaskComponent },
  {path:"app-edittask/:id",component:EdittaskComponent},
  {path:"app-listtask",component:ListtaskComponent},
  {path:"app-viewtask",component:ViewtaskComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routerComponents =[AddTaskComponent,
                                EdittaskComponent,
                                ViewtaskComponent,
                                ListtaskComponent
                                ]
